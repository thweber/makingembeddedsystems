#include "battery.h"
#include <stdint.h>

static uint8_t (*getCapacity)(void);
static int16_t (*getTemperature)(void);

void batteryInit(uint8_t (*fnGetCapacity)(void))
{
	getCapacity = fnGetCapacity;
}

int16_t batteryGetTemperature(void)
{
	int16_t temperature;

	return temperature;
}

uint16_t batteryGetTimeRemaining(void)
{
	uint16_t time;

	return time;
}

enum batteryHealth batteryGetStatus(void)
{
	enum batteryHealth status;

	return status;
}

uint8_t batteryGetCapacity(void)
{
	uint8_t capacity = 0;
	
	capacity = getCapacity();

	return capacity;
}
