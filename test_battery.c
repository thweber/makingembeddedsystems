#include <stdio.h>

#include "battery.h"

uint8_t sendCapacity(void)
{
	return 98;
}

void main(void)
{
	batteryInit(sendCapacity);
	printf("battery capacity: %d\n", batteryGetCapacity());
}
