#ifndef BATTERY_H
#define BATTERY_H

#include <stdint.h>

enum batteryHealth {batteryGood = 0, batteryBad = 1};

void batteryInit(uint8_t (*fnGetCapacity)(void));
uint8_t batteryGetCapacity(void);
#endif
